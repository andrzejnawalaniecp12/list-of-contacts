import "./App.scss";
import Header from "./components/Header/Header";
import ContactList from "./containers/ContactList/ContactList";

function App() {
  return (
    <div className="app">
      <Header />
      <ContactList />
    </div>
  );
}

export default App;
