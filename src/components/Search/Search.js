import React from "react";

import "./Search.scss";

import searchIcon from "../../assets/img/search.png";

const Search = props => (
  <div className="search">
    <div className="search__icon-wrapper">
      <img className="search__icon" src={searchIcon} alt="search" />
    </div>
    <input className="search__input" type="text" onChange={props.onChange} />
  </div>
);

export default Search;
