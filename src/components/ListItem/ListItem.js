import React from "react";

import "./ListItem.scss";

const ListItem = props => (
  <div className="list-item">
    <label htmlFor={props.id} className="list-item__content">
      <div className="list-item__avatar-wrapper">
        {props.avatar ? (
          <img className="list-item__avatar" src={props.avatar} alt="user avatar" />
        ) : (
          <div className="list-item__avatar-placeholder">
            {props.firstName.charAt(0)} {props.lastName.charAt(0)}
          </div>
        )}
      </div>
      <div className="list-item__info">
        <div className="list-item__name">
          <p className="list-item__name-text">
            {props.firstName} {props.lastName}
          </p>
        </div>
        <div className="list-item__contact">
          <p className="list-item__contact-email">{props.email}</p>
        </div>
      </div>

      <div className="list-item__checkbox-wrapper">
        <input
          className="list-item__checkbox"
          type="checkbox"
          id={props.id}
          onChange={event => props.onChange(event, props.id)}
          defaultChecked={props.checked}
        />
      </div>
    </label>
  </div>
);

export default ListItem;
