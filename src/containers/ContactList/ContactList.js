import React, { Component } from "react";
import ListItem from "../../components/ListItem/ListItem";
import Spinner from "../../components/Spinner/Spinner";
import Search from "../../components/Search/Search";

import "./ContactList.scss";

class ContactList extends Component {
  state = {
    contacts: [],
    searchContacts: [],
    checkedContactIDs: [],
    loading: true,
    searching: false,
    currentPage: 1,
    contactsPerPage: 20,
  };

  componentDidMount() {
    fetch("https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json")
      .then(response => response.json())
      .then(data => {
        data.sort(function (a, b) {
          return a.last_name.localeCompare(b.last_name);
        });
        this.setState({ contacts: data, loading: false });
      });
  }

  handleContactCheck = (event, id) => {
    let newCheckedContactIDs = [...this.state.checkedContactIDs];

    if (event.target.checked) newCheckedContactIDs.push(id);
    else newCheckedContactIDs = newCheckedContactIDs.filter(contactID => contactID !== id);

    this.setState({ checkedContactIDs: newCheckedContactIDs });

    console.log(newCheckedContactIDs);
  };

  handleSearch = event => {
    const searchedPhrase = event.target.value;

    const newSearchContacts = this.state.contacts.filter(contact => {
      const fullName = `${contact.first_name} ${contact.last_name}`;
      return fullName.toLowerCase().includes(searchedPhrase.toLowerCase());
    });

    let newSearching = searchedPhrase.trim().length > 0 ? true : false;

    this.setState({ searchContacts: newSearchContacts, searching: newSearching, currentPage: 1 });
  };

  handlePageChange = number => {
    this.setState({ currentPage: number });
  };

  render() {
    const contacts = this.state.contacts.map(contact => {
      return (
        <ListItem
          key={contact.id}
          id={contact.id}
          avatar={contact.avatar}
          firstName={contact.first_name}
          lastName={contact.last_name}
          email={contact.email}
          onChange={this.handleContactCheck}
          checked={this.state.checkedContactIDs.includes(contact.id)}
        />
      );
    });

    const searchedContacts = this.state.searchContacts.map(contact => {
      return (
        <ListItem
          key={contact.id}
          id={contact.id}
          avatar={contact.avatar}
          firstName={contact.first_name}
          lastName={contact.last_name}
          email={contact.email}
          onChange={this.handleContactCheck}
          checked={this.state.checkedContactIDs.includes(contact.id)}
        />
      );
    });

    const contactsToRender = this.state.searching ? searchedContacts : contacts;

    //Pagination
    const { currentPage, contactsPerPage } = this.state;

    const indexOfLastContact = currentPage * contactsPerPage;
    const indexOfFirstContact = indexOfLastContact - contactsPerPage;
    const currentContactsToRender = contactsToRender.slice(indexOfFirstContact, indexOfLastContact);

    const pageNumbers = [];
    for (let i = 1; i <= Math.ceil(contactsToRender.length / contactsPerPage); i++) {
      pageNumbers.push(i);
    }

    const pageNumbersToRender = pageNumbers.map(number => {
      return (
        <li
          className={`contact-list__page-numbers-item ${
            currentPage === number ? "contact-list__page-numbers-item--active" : ""
          }`}
          key={number}
          onClick={() => this.handlePageChange(number)}>
          {number}
        </li>
      );
    });

    return (
      <div className="contact-list">
        <Search phrase={this.state.searchedPhrase} onChange={this.handleSearch} />
        {this.state.loading ? <Spinner /> : currentContactsToRender}
        <ul className="contact-list__page-numbers">{pageNumbersToRender}</ul>
      </div>
    );
  }
}

export default ContactList;
